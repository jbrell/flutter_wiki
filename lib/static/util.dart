import 'dart:io';

import 'package:flutter/foundation.dart';

final isMobile = !kIsWeb && (Platform.isAndroid || Platform.isIOS);

const appName = 'flutter_wiki';
const appTitle = 'Flutter Wiki';

const noSearchResult =
    'Zu deiner Suchanfrage wurden keine Ergebnisse gefunden.';

const search = 'Durchsuche';

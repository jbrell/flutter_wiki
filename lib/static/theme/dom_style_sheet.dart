import 'package:flutter/material.dart';
import 'package:flutter_html/style.dart';
import 'package:flutter_wiki/gears/extensions.dart';
import 'package:google_fonts/google_fonts.dart';

Map<String, Style> of(BuildContext context) {
  final colors = context.appColors;
  return {
    'body': Style(
      color: colors.foreground,
      backgroundColor: colors.background,
      fontSize: FontSize.medium,
      fontFamily: GoogleFonts.rubik().fontFamily,
      fontFamilyFallback: [
        GoogleFonts.notoSans().fontFamily,
        GoogleFonts.openSans().fontFamily,
      ].whereType<String>().toList(),
      fontWeight: FontWeight.w500,
      lineHeight: const LineHeight(1.75),
      padding: const EdgeInsets.all(16),
    ),
    'h1': Style(
      margin: Margins.only(bottom: 20),
      fontSize: FontSize(36),
      fontWeight: FontWeight.w300,
      letterSpacing: 0.25,
      lineHeight: const LineHeight(1.25),
    ),
    'h2': Style(
      margin: Margins.only(top: 18, bottom: 9),
      padding: const EdgeInsets.symmetric(vertical: 6),
      border: Border(
        bottom: BorderSide(color: colors.horizontalRule),
      ),
      // fontFamily: GoogleFonts.rubik().fontFamily,
      fontSize: FontSize(24),
      fontWeight: FontWeight.w300,
      letterSpacing: 0.25,
      lineHeight: const LineHeight(1.25),
    ),
    'p': Style(
      margin: Margins.only(bottom: 24),
      fontSize: FontSize(16),
    ),
    'a': Style(
      color: colors.anchor,
      textDecoration: TextDecoration.none,
    ),
    'a:hover': Style(
      color: colors.anchorHover,
      textDecoration: TextDecoration.underline,
    ),
    'a:active': Style(
      color: colors.anchorActive,
      textDecoration: TextDecoration.underline,
    ),
    'a:visited': Style(
      color: colors.anchorVisited,
      textDecoration: TextDecoration.none,
    ),
    'a:visited:hover': Style(
      color: colors.anchorVisitedHover,
      textDecoration: TextDecoration.underline,
    ),
    'a:visited:active': Style(
      color: colors.anchorVisitedActive,
      textDecoration: TextDecoration.underline,
    ),
  };
}

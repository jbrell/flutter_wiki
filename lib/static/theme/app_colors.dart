import 'package:flutter/material.dart';

class AppColors {
  const AppColors({
    required this.brightness,
    required this.background,
    required this.foreground,
    Color? anchor,
    Color? anchorHover,
    Color? anchorActive,
    Color? anchorVisited,
    Color? anchorVisitedHover,
    Color? anchorVisitedActive,
    Color? horizontalRule,
  })  : anchor = anchor ?? foreground,
        anchorHover = anchorHover ?? anchor ?? foreground,
        anchorActive = anchorActive ?? anchorHover ?? anchor ?? foreground,
        anchorVisited = anchorVisited ?? anchor ?? foreground,
        anchorVisitedHover =
            anchorVisitedHover ?? anchorVisited ?? anchor ?? foreground,
        anchorVisitedActive = anchorVisitedActive ??
            anchorVisitedHover ??
            anchorVisited ??
            anchor ??
            foreground,
        horizontalRule = horizontalRule ?? foreground;

  static const defaultLight = AppColors(
    brightness: Brightness.light,
    background: Colors.white,
    foreground: Color(0xff3a3a3a),
    anchor: Color(0xff006cb0),
    anchorHover: Color(0xff002d4a),
    anchorActive: Color(0xff001f33),
    anchorVisited: Color(0xff442673),
    anchorVisitedHover: Color(0xff6a2673),
    anchorVisitedActive: Color(0xff732655),
    horizontalRule: Color(0xffcecece),
  );

  static const defaultDark = AppColors(
    brightness: Brightness.dark,
    background: Color(0xff0e191a),
    foreground: Color(0xffe6e6e6),
    anchor: Color(0xff00cdd0),
    anchorHover: Color(0xff37fcff),
    anchorActive: Color(0xff75fdff),
    anchorVisited: Color(0xff0065d0),
    anchorVisitedHover: Color(0xff3396ff),
    anchorVisitedActive: Color(0xff75b8ff),
    horizontalRule: Color(0xff444c4d),
  );

  final Brightness brightness;
  final Color background;
  final Color foreground;
  final Color anchor;
  final Color anchorHover;
  final Color anchorActive;
  final Color anchorVisited;
  final Color anchorVisitedHover;
  final Color anchorVisitedActive;
  final Color horizontalRule;
}

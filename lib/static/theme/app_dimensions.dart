mixin AppDimensions {
  static const searchBarHeight = 56.0;

  // static const halfPadding = 4.0;
  static const defaultPadding = 8.0;
  static const doublePadding = 16.0;
  static const inputPadding = 16.0;

  static const textFieldBorderRadius = 8.0;
  static const shadowBlurRadius = 4.0;
}

import 'package:flutter/material.dart';
import 'package:flutter_wiki/data/article_repository.dart';
import 'package:flutter_wiki/gears/extensions.dart';
import 'package:flutter_wiki/gears/initializers/init_injectable.dart';
import 'package:flutter_wiki/static/theme/app_dimensions.dart';
import 'package:flutter_wiki/static/translations.dart' as translations;
import 'package:flutter_wiki/static/util.dart';
import 'package:flutter_wiki/ui/components/sliding_search_bar.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  String? _article = getIt<ArticleRepository>()['example'];
  late final AnimationController _animationController;
  late final Animation<Offset> _offsetAnimation;
  bool _appBarIsVisible = true;

  @override
  void initState() {
    _animationController = AnimationController(
      duration: const Duration(milliseconds: 200),
      vsync: this,
    )..addListener(() => setState(() {}));
    _offsetAnimation = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(0, -1),
    ).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Curves.fastOutSlowIn,
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: SlidingSearchBar(
          animation: _offsetAnimation,
          onSubmitted: searchArticle,
          hintText: _searchBarHintText,
        ),
        body: Scrollbar(
          controller: context.primaryScrollController,
          thumbVisibility: !isMobile,
          child: SingleChildScrollView(
            controller: context.primaryScrollController,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: AppDimensions.defaultPadding,
              ),
              child: Text(_article!),
              // child: ArticleView(article: _article),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: toggleSearchBar,
          mini: true,
          child: const Icon(Icons.search),
        ),
      ),
    );
  }

  void searchArticle(String query) {
    setState(() {
      _article = getIt<ArticleRepository>()[query];
    });
  }

  void toggleSearchBar() {
    setState(() {
      _appBarIsVisible
          ? _animationController.reverse()
          : _animationController.forward();
      _appBarIsVisible ^= true;
    });
  }

  void hideSearchBar() {
    if (_appBarIsVisible) {
      toggleSearchBar();
    }
  }
}

const _searchBarHintText = '${translations.search} ${translations.appTitle}…';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_wiki/gears/extensions.dart';

class Hyperlink extends StatefulWidget {
  const Hyperlink({
    required super.key,
    required this.renderContext,
    required this.buildChildren,
    Curve? scrollCurve,
    Duration? scrollDuration,
  })  : scrollCurve = scrollCurve ?? Curves.easeInOut,
        scrollDuration = scrollDuration ?? const Duration(milliseconds: 250);

  static const tag = 'a';

  final RenderContext renderContext;
  final List<InlineSpan> Function() buildChildren;
  final Curve scrollCurve;
  final Duration scrollDuration;

  static CustomRender customRender({
    Curve? scrollCurve,
    Duration? scrollDuration,
  }) =>
      CustomRender.inlineSpan(
        inlineSpan: (renderContext, buildChildren) => WidgetSpan(
          alignment: PlaceholderAlignment.baseline,
          baseline: TextBaseline.alphabetic,
          child: Hyperlink(
            key: AnchorKey.of(renderContext.parser.key, renderContext.tree),
            renderContext: renderContext,
            buildChildren: buildChildren,
            scrollCurve: scrollCurve,
            scrollDuration: scrollDuration,
          ),
        ),
      );

  @override
  State<Hyperlink> createState() => _HyperlinkState();
}

class _HyperlinkState extends State<Hyperlink> {
  late final TapGestureRecognizer _tapGestureRecognizer;
  bool visited = false;
  bool hover = false;
  bool active = false;

  RenderContext get renderContext => widget.renderContext;

  InteractableElement get tree => renderContext.tree as InteractableElement;

  HtmlParser get parser => renderContext.parser;

  @override
  void initState() {
    super.initState();
    _tapGestureRecognizer = TapGestureRecognizer()
      ..onTap = onTap
      ..onTapDown = (_) {
        setState(() => active = true);
      }
      ..onTapUp = (_) {
        setState(() => active = false);
      }
      ..onTapCancel = () {
        setState(() => active = false);
      };
  }

  @override
  void dispose() {
    _tapGestureRecognizer.dispose();
    super.dispose();
  }

  void onTap() {
    final url = tree.href;
    if (url != null && url.startsWith('#')) {
      final anchorContext =
          AnchorKey.forId(parser.key, url.substring(1))?.currentContext;
      if (anchorContext != null) {
        Scrollable.ensureVisible(
          anchorContext,
          curve: widget.scrollCurve,
          duration: widget.scrollDuration,
        );
      }
    } else {
      parser.onLinkTap?.call(url, renderContext, tree.attributes, tree.element);
    }
    setState(() => visited = true);
  }

  @override
  Widget build(BuildContext context) {
    final style = styleOf(context);
    return CssBoxWidget.withInlineSpanChildren(
      style: style,
      shrinkWrap: parser.shrinkWrap,
      children: [
        TextSpan(
          text: tree.element?.innerHtml,
          style: style.generateTextStyle(),
          recognizer: _tapGestureRecognizer,
          onEnter: (_) => setState(() => hover = true),
          onExit: (_) => setState(() => hover = false),
        ),
      ],
    );
  }

  Style styleOf(BuildContext context) {
    Style applyPseudoClasses({
      String? pseudoClass,
      Iterable<String>? pseudoClasses,
    }) =>
        tree.style.applyPseudoClasses(
          styleSheet: context.styleSheet,
          element: Hyperlink.tag,
          pseudoClass: pseudoClass,
          pseudoClasses: pseudoClasses,
        );
    if (visited && active) {
      return applyPseudoClasses(
        pseudoClasses: ['visited', 'active', 'visited:active'],
      );
    }
    if (visited && hover) {
      return applyPseudoClasses(
        pseudoClasses: ['visited', 'hover', 'visited:hover'],
      );
    }
    if (visited) {
      return applyPseudoClasses(
        pseudoClass: 'visited',
      );
    }
    if (active) {
      return applyPseudoClasses(
        pseudoClasses: ['link', 'active', 'link:active'],
      );
    }
    if (hover) {
      return applyPseudoClasses(
        pseudoClasses: ['link', 'hover', 'link:hover'],
      );
    }
    return applyPseudoClasses(
      pseudoClass: 'link',
    );
  }
}

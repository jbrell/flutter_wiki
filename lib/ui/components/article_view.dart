import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_wiki/gears/extensions/on_build_context.dart';
import 'package:flutter_wiki/static/translations.dart' as translations;
import 'package:flutter_wiki/ui/dom_elements/hyper_link.dart';

class ArticleView extends StatelessWidget {
  const ArticleView({
    this.article,
    super.key,
  });

  final String? article;

  @override
  Widget build(BuildContext context) {
    return article == null
        ? Text(article ?? translations.noSearchResult)
        : SelectionArea(
            child: Html(
              data: article,
              style: context.styleSheet,
              customRenders: {
                tagMatcher(Hyperlink.tag): Hyperlink.customRender(),
              },
            ),
          );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_wiki/gears/extensions.dart';
import 'package:flutter_wiki/static/theme/app_dimensions.dart';

class SearchField extends StatefulWidget {
  const SearchField({
    this.onSubmitted,
    this.hintText,
    super.key,
  });

  final void Function(String)? onSubmitted;
  final String? hintText;

  @override
  State<SearchField> createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  final TextEditingController controller = TextEditingController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        // color: context.colorScheme.surface,
        // TODO: Define app ColorScheme
        color: Colors.grey[800],
        borderRadius: BorderRadius.circular(
          AppDimensions.textFieldBorderRadius,
        ),
        boxShadow: [
          BoxShadow(
            color: context.colorScheme.shadow,
            blurRadius: AppDimensions.shadowBlurRadius,
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(AppDimensions.defaultPadding),
        child: TextField(
          onSubmitted: widget.onSubmitted,
          controller: controller,
          style: context.textTheme.bodyLarge,
          // TODO: Replace with more appropriate color.
          cursorColor: context.colorScheme.outline,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            hintText: widget.hintText,
            border: InputBorder.none,
            suffixIcon: Icon(
              Icons.search,
              color: context.theme.hintColor,
            ),
          ),
        ),
      ),
    );
  }
}

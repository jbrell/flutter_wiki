import 'package:flutter/material.dart';
import 'package:flutter_wiki/gears/extensions.dart';
import 'package:flutter_wiki/static/theme/app_dimensions.dart';
import 'package:flutter_wiki/ui/components/search_field.dart';

class SlidingSearchBar extends StatelessWidget with PreferredSizeWidget {
  const SlidingSearchBar({
    required this.animation,
    required this.onSubmitted,
    this.hintText,
    this.height = AppDimensions.searchBarHeight,
    super.key,
  });

  final Animation<Offset> animation;
  final void Function(String)? onSubmitted;
  final String? hintText;
  final double height;

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    final inputFieldHeight =
        (context.textTheme.bodyLarge?.height ?? 16.0) * 2.5;
    return SlideTransition(
      position: animation,
      child: ColoredBox(
        // TODO: Replace with appropriate Color.
        color: context.appColors.background,
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: AppDimensions.inputPadding,
            vertical: (preferredSize.height - inputFieldHeight) / 2,
          ),
          // color: context.colorScheme.primary,
          child: SizedBox(
            height: inputFieldHeight,
            child: SearchField(
              onSubmitted: onSubmitted,
              hintText: hintText,
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_wiki/static/translations.dart' as translations;
import 'package:flutter_wiki/ui/main_page.dart';

class WikiApp extends StatelessWidget {
  const WikiApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: translations.appTitle,
      theme: ThemeData(
        // TODO: Define default ColorSchemes.
        colorScheme:
            const ColorScheme.dark() /*.copyWith(surface: Colors.grey[800])*/,
      ),
      home: const MainPage(),
    );
  }
}

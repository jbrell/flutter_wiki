import 'dart:collection';

import 'package:hive/hive.dart';

class ArticleRepository with MapMixin<String, String> {
  ArticleRepository._();

  static const boxName = 'articles';

  late final Box<String> _box;

  static Future<ArticleRepository> create() async {
    final instance = ArticleRepository._();
    await instance._init();
    return instance;
  }

  static String encodeFileName(String name) => Uri.encodeFull(name);

  static String decodeFileName(String name) => Uri.decodeFull(name);

  Future<void> _init() async {
    _box = await Hive.openBox<String>(boxName);
  }

  @override
  String? operator [](covariant String key) => _box.get(key);

  @override
  void operator []=(String key, String value) => _box.put(key, value);

  @override
  bool containsKey(covariant String key) => _box.containsKey(key);

  @override
  void clear() => _box.clear();

  @override
  Iterable<String> get keys => _box.keys.cast();

  @override
  String? remove(Object? key) {
    final value = _box.get(key);
    _box.delete(key);
    return value;
  }
}

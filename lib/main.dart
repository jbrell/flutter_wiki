import 'package:flutter/material.dart';
import 'package:flutter_wiki/gears/initializers.dart';
import 'package:flutter_wiki/ui/wiki_app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initInjectable();
  await updateDatabase(/*updateAll: true*/);
  registerFontLicenses();

  runApp(const WikiApp());
}

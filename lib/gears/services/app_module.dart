import 'package:flutter/foundation.dart';
import 'package:flutter_wiki/data/article_repository.dart';
import 'package:flutter_wiki/static/translations.dart' as translations;
import 'package:hive_flutter/hive_flutter.dart';
import 'package:injectable/injectable.dart';
import 'package:path_provider/path_provider.dart';

@module
abstract class AppModule {
  bool _initializedHive = false;

  @preResolve
  Future<ArticleRepository> get articleRepository async {
    if (!_initializedHive) {
      await _initHive();
    }
    return ArticleRepository.create();
  }

  Future<void> _initHive() async {
    final dbPath = kIsWeb
        ? translations.appName
        : (await getApplicationSupportDirectory()).path;
    await Hive.initFlutter(dbPath);
    _initializedHive = true;
  }
}

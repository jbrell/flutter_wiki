import 'package:flutter_wiki/gears/markdown/inline_syntaxes.dart';
import 'package:markdown/markdown.dart' as md;

String markdownToHtml(String markdown) {
  return md.markdownToHtml(
    markdown,
    inlineSyntaxes: [
      FootnoteSyntax(),
      HighlightSyntax(),
      StriketoutSyntax(),
      SuperscriptSyntax(),
      SubscriptSyntax(),
    ],
    extensionSet: md.ExtensionSet.gitHubWeb,
    encodeHtml: false,
  );
}

import 'package:markdown/markdown.dart';

/// Matches strikeout syntax according to the [pandoc spec]
/// (https://pandoc.org/MANUAL.html#strikeout)
///
/// The [StrikethroughSyntax] from the markdown package
/// conflicts with [SubscriptSyntax].
class StriketoutSyntax extends DelimiterSyntax {
  StriketoutSyntax()
      : super(
          '~~',
          requiresDelimiterRun: true,
          allowIntraWord: true,
          tags: [DelimiterTag('del', 2)],
        );
}

import 'package:markdown/markdown.dart';
import 'package:markdown/src/charcode.dart'; // ignore: implementation_imports

/// Matches footnote syntax according to the [pandoc spec]
/// (https://pandoc.org/MANUAL.html#footnotes)
class FootnoteSyntax extends LinkSyntax {
  FootnoteSyntax()
      : super(
          pattern: r'\[\^',
          startCharacter: $lbracket,
        );

  /// Generates a valid HTML anchor from a string.
  static String generateAnchorHash(String text) => text
      .toLowerCase()
      .trim()
      .replaceAll(RegExp('[^a-z0-9 _-]'), '')
      .replaceAll(RegExp(r'\s'), '-');

  @override
  Node? close(
    InlineParser parser,
    covariant SimpleDelimiter opener,
    Delimiter? closer, {
    String? tag,
    required List<Node> Function() getChildren,
  }) {
    final reference = generateAnchorHash(
      parser.source.substring(opener.endPos, parser.pos),
    );
    // Peek at the next character to see, whether it is a `:`.
    if (parser.pos + 1 < parser.source.length &&
        parser.charAt(parser.pos + 1) == $colon) {
      parser.advanceBy(1);
      return createFootnote(reference, getChildren: getChildren);
    }
    return createReference(reference, getChildren: getChildren);
  }

  /// Create the node represented by a Markdown link.
  Node createReference(String reference, {List<Node> Function()? getChildren}) {
    final link = Element('a', getChildren?.call())
      ..attributes['href'] = '#footnote-$reference';
    return Element('sup', [link])
      ..attributes['id'] = 'reference-$reference'
      ..attributes['class'] = 'reference';
  }

  Node createFootnote(String reference, {List<Node> Function()? getChildren}) {
    return Element('span', [
      ...?getChildren?.call(),
      // Jump to anchor seems to work only with <sub> and <sup>??
      Element('sup', [
        Element.text('a', '↑')..attributes['href'] = '#reference-$reference',
      ])
        ..attributes['id'] = 'footnote-$reference',
    ])
      ..attributes['class'] = 'footnote';
  }
}

import 'package:markdown/markdown.dart';

/// Matches subscript syntax according to the [pandoc spec]
/// (https://pandoc.org/MANUAL.html#superscripts-and-subscripts)
class SubscriptSyntax extends DelimiterSyntax {
  SubscriptSyntax()
      : super(
          '~',
          requiresDelimiterRun: true,
          allowIntraWord: true,
          tags: [DelimiterTag('sub', 1)],
        );
}

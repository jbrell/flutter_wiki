import 'package:markdown/markdown.dart';

/// Matches highlight syntax found [here]
/// (https://www.w3schools.io/file/markdown-text-highlight/)
class HighlightSyntax extends DelimiterSyntax {
  HighlightSyntax()
      : super(
          '==',
          requiresDelimiterRun: true,
          allowIntraWord: true,
          tags: [DelimiterTag('mark', 2)],
        );
}

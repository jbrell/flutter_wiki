import 'package:markdown/markdown.dart';

/// Matches superscript syntax according to the [pandoc spec]
/// (https://pandoc.org/MANUAL.html#superscripts-and-subscripts)
class SuperscriptSyntax extends DelimiterSyntax {
  SuperscriptSyntax()
      : super(
          r'\^',
          requiresDelimiterRun: true,
          allowIntraWord: true,
          tags: [DelimiterTag('sup', 1)],
        );
}

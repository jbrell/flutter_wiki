import 'package:flutter/material.dart';
import 'package:flutter_wiki/static/theme/app_colors.dart';

extension OnColorScheme on ColorScheme {
  static final _appColors = Expando<AppColors>();

  AppColors get appColors =>
      _appColors[this] ??
      (isLight ? AppColors.defaultLight : AppColors.defaultDark);

  set appColors(AppColors appColors) => _appColors[this] = appColors;

  bool get isLight {
    switch (brightness) {
      case Brightness.light:
        return true;
      case Brightness.dark:
        return false;
    }
  }
}

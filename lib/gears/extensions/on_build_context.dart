import 'package:flutter/material.dart';
import 'package:flutter_html/style.dart';
import 'package:flutter_wiki/gears/extensions.dart';
import 'package:flutter_wiki/static/theme/app_colors.dart';
import 'package:flutter_wiki/static/theme/dom_style_sheet.dart' as style_sheet;

extension OnBuildContext on BuildContext {
  ThemeData get theme => Theme.of(this);

  ColorScheme get colorScheme => theme.colorScheme;

  TextTheme get textTheme => theme.textTheme;

  TextStyle get defaultTextStyle => DefaultTextStyle.of(this).style;

  AppColors get appColors => theme.colorScheme.appColors;

  Map<String, Style> get styleSheet => style_sheet.of(this);

  ScrollController? get primaryScrollController =>
      PrimaryScrollController.of(this);
}

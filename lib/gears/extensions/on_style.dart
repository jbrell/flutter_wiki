import 'package:flutter_html/flutter_html.dart';

extension OnStyle on Style {
  Style applyPseudoClasses({
    required Map<String, Style> styleSheet,
    required String element,
    String? pseudoClass,
    Iterable<String>? pseudoClasses,
  }) {
    return [
      if (pseudoClass != null) pseudoClass,
      if (pseudoClasses != null) ...pseudoClasses,
    ].fold(
      this,
      (Style style, String selector) =>
          style.maybeMerge(styleSheet['$element:$selector']),
    );
  }

  Style maybeMerge(Style? other) {
    return other == null ? this : merge(other);
  }
}

export 'initializers/init_injectable.dart';
export 'initializers/register_font_licenses.dart';
export 'initializers/update_database.dart';

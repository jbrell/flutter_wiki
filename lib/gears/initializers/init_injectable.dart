import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

// ignore: always_use_package_imports
import 'init_injectable.config.dart';

final GetIt getIt = GetIt.instance;

@InjectableInit()
Future<void> initInjectable({String? environment}) async {
  await $initGetIt(getIt, environment: environment);
}

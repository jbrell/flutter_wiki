import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_wiki/data/article_repository.dart';
import 'package:flutter_wiki/gears/initializers.dart';
import 'package:flutter_wiki/gears/markdown/html_renderer.dart';
import 'package:flutter_wiki/static/asset_path.dart';

Future<void> updateDatabase({bool updateAll = false}) async {
  final assetManifest =
      json.decode(await rootBundle.loadString('AssetManifest.json'))
          as Map<String, dynamic>;
  final repository = getIt<ArticleRepository>();
  final filenames = assetManifest.keys.where(
    (String filename) => filename.startsWith(AssetPath.articles),
  );
  for (final filename in filenames) {
    assert(filename.endsWith('.md'));
    final articleName = ArticleRepository.decodeFileName(
      filename.substring(
        AssetPath.articles.length,
        filename.length - 3,
      ),
    );
    if (updateAll || !repository.containsKey(articleName)) {
      repository[articleName] = '<div id="top"/>'
          '${markdownToHtml(await rootBundle.loadString(filename))}'
          '<div id="bottom"/>';
    }
  }
}

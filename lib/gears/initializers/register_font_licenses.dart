import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

void registerFontLicenses() {
  const fontLicenseFiles = [
    'assets/fonts/google_fonts/Rubik/OFL.txt',
    'assets/fonts/google_fonts/Spline_Sans_Mono/OFL.txt',
  ];

  for (final filepath in fontLicenseFiles) {
    LicenseRegistry.addLicense(() async* {
      final license = await rootBundle.loadString(filepath);
      yield LicenseEntryWithLineBreaks(['google_fonts'], license);
    });
  }
}

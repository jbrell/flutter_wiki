# Great Example

Lorem ipsum **dolor** sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et **dolor**e magna aliqua. Malesuada fames ac turpis egestas maecenas pharetra convallis posuere. Nunc vel risus commodo viverra maecenas accumsan. Pellentesque elit ullamcorper dignissim cras. Lacus viverra vitae congue eu. Gravida quis blandit turpis cursus in hac habitasse platea. Rhoncus mattis rhoncus urna neque viverra justo. Malesuada fames ac turpis egestas maecenas. Ac turpis egestas sed tempus. Aliquet eget sit amet tellus cras. Sagittis vitae et leo duis ut diam. Laoreet non curabitur gravida arcu ac tortor dignissim convallis. Quis ipsum suspendisse ultrices gravida dictum fusce. Sed turpis tincidunt id aliquet.

## This paragraph has the first footnote in the end

Id consectetur purus ut faucibus pulvinar elementum integer. Aliquam etiam erat velit scelerisque in dictum non consectetur a. Sit amet aliquam id diam maecenas ultricies mi eget. Ac tincidunt vitae semper quis lectus. Turpis massa sed elementum tempus. Ornare massa eget egestas purus. Arcu non sodales neque sodales ut etiam sit. Sit amet tellus cras adipiscing enim eu turpis. Lectus vestibulum mattis ullamcorper velit sed ullamcorper. Ultrices eros in cursus turpis. Feugiat in fermentum posuere urna nec tincidunt praesent semper. Risus in hendrerit gravida rutrum quisque non.[^1]

## These special syntax are not recognized by the dart markdown package

Support for Emojis! :joy: \
I need to ==highlight== this.\
Always drink enough H~2~O to support your health.\
The equation E = m · c^2^ is fundamental in physics.\
There should be a linebreak between this line\
and this one.

## Some JSON code example

Nunc pulvinar sapien et ligula ullamcorper malesuada proin. Fermentum posuere urna nec tincidunt praesent semper feugiat nibh. Nunc faucibus a pellentesque sit amet porttitor eget. Blandit libero volutpat sed cras ornare arcu dui. Montes nascetur ridiculus mus mauris.

```
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```

Varius quam quisque id diam vel quam elementum pulvinar. Mattis nunc sed blandit libero. Lorem ipsum **dolor** sit amet. Natoque penatibus et magnis dis parturient montes nascetur ridiculus mus. Egestas diam in arcu cursus euismod quis viverra. Etiam erat velit scelerisque in dictum non consectetur a. Purus sit amet volutpat consequat mauris nunc congue nisi.

### Here we go with a second footnote, this thime in the middle of the paragraph

Placerat in egestas erat imperdiet sed euismod nisi porta. Varius vel pharetra vel turpis nunc eget lorem **dolor** sed. ~~condimentum~~ id venenatis a ~~condimentum~~ vitae sapien pellentesque habitant morbi.[^2] Arcu bibendum at varius vel pharetra vel turpis nunc. Consequat ac felis donec et odio pellentesque diam volutpat.

### Linebreaks inside a paragraph

Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant morbi.<br>
Consequat mauris nunc congue nisi vitae suscipit tellus mauris. In nisl nisi scelerisque eu.<br>
Rhoncus mattis rhoncus urna neque viverra justo.<br>Lacus viverra vitae congue eu consequat ac felis.<br>
Magna sit amet purus gravida. Facilisis gravida neque convallis a cras semper auctor neque.<br>Quis vel eros donec ac odio.

### This paragraph has an enumeration in the middle

Ut eu sem integer vitae justo. Tellus rutrum tellus pellentesque eu. Est ante in nibh mauris cursus mattis molestie a. Mattis rhoncus urna neque viverra justo nec.

1. First item
  1. FirstFirst item
  2. FirstSecond item
    1. FirstSecondFirst item
    2. FirstSecondSecond item
2. Second item
3. Third item

Dictum sit amet justo donec enim diam vulputate ut pharetra.

## Something with a quote

Sit amet dictum sit amet justo donec enim. Sed libero enim sed faucibus turpis in eu mi. Egestas quis ipsum suspendisse ultrices gravida dictum fusce ut.

> Twas bryllyg, and ye slythy toves
Did gyre and gymble in ye wabe:
All mimsy were ye borogoves;
And ye mome raths outgrabe.[^3]

Vehicula ipsum a arcu cursus vitae. Sit amet risus _nulla_m eget felis eget nunc lobortis. In vitae turpis massa sed elementum.

### Here comes some inline code

Est ullamcorper eget _nulla_ facilisi etiam. Fames ac turpis egestas integer eget aliquet nibh praesent. `System.out.println("Hello World!");` Praesent elementum facilisis leo vel fringilla est ullamcorper eget _nulla_. Diam quis enim lobortis scelerisque fermentum dui faucibus in ornare. **dolor** morbi non arcu risus quis varius quam quisque. 

### This Paragraph should have code with syntax highlighting

Imperdiet _nulla_ malesuada pellentesque elit eget gravida cum sociis. Sed tempus urna et pharetra pharetra. Consectetur lorem donec massa sapien faucibus et molestie ac. Erat velit scelerisque in dictum non consectetur a erat nam.
```dart
import 'dart:ui' as ui;

ui.Image image;

void paint(Canvas canvas, Size size) {
  canvas.drawImage(
    image,
    Offset.zero,
    Paint()..imageFilter = ui.ImageFilter.blur(sigmaX: .5, sigmaY: .5),
  );
}
```
Fermentum iaculis eu non diam phasellus vestibulum lorem sed. Purus sit amet volutpat consequat. Elementum integer enim neque volutpat ac tincidunt vitae semper quis. Aliquet nibh praesent tristique magna. Proin sagittis nisl rhoncus mattis rhoncus urna neque viverra justo.

#### How to define a term?

Massa tincidunt dui ut ornare lectus sit amet est. Elementum sagittis vitae et leo. Eget lorem **dolor** sed viverra. Consectetur adipiscing elit pellentesque habitant morbi tristique senectus. Augue mauris augue neque gravida in fermentum et. Montes nascetur ridiculus mus mauris vitae.

term
: definition
another term
: and another definition

<dl>
  <dt>Lower cost</dt>
  <dd>The new version of this product costs significantly less than the previous one!</dd>
  <dt>Easier to use</dt>
  <dd>We've changed the product so that it's much easier to use!</dd>
</dl>

##### Do we really need a h5 heading?

##### Or even h6?

Velit aliquet sagittis id consectetur purus ut faucibus. Leo integer malesuada nunc vel risus commodo viverra maecenas accumsan. Consequat id porta nibh venenatis cras sed felis eget velit. Amet volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Aliquam eleifend mi in _nulla_ posuere. _nulla_ facilisi cras fermentum odio eu feugiat pretium. Morbi tristique senectus et netus et malesuada.

##### This level 6 heading announces a table with alignment

| Item         | Price | # In stock |
|--------------|:-----:|-----------:|
| Juicy Apples |  1.99 |        739 |
| Bananas      |  1.89 |          6 |

#### Maybe not so useful in a wiki, but what who knows...?

Elementum eu facilisis sed odio morbi quis commodo odio. Eget _nulla_m non nisi est.

- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media

Phasellus vestibulum lorem sed risus ultricies tristique _nulla_ aliquet. Ut aliquam purus sit amet luctus venenatis. Morbi leo urna molestie at elementum eu facilisis.

## See also

- First item
  - FirstFirst item
  - FirstSecond item
- Second item
  - SecondFirst item
    - FirstSecondFirst item
  - SecondSecond item
- Third item
- Fourth item

---

## References

### Footnotes {#custom-id}

[^1]: This text was generated by [loremipsum.io](https://loremipsum.io).

[^2]: Another reference, but leads intern to [Something with a quote](#something-with-a-quote).

[//]: # (TODO: anchor to 'top' instead of <article name>)
[^3]: This link brings you back to the [top](#top).
